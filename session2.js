
const bcrypt = require('bcrypt')
const request = require('supertest-as-promised')

class SessionApp {
  // login (email, password, app) {
  login (email, password, app, role) {
    console.log('Email: ', email)
    console.log('Password: ', password)
    console.log('App: ', app)
    console.log('role: ', role)
    return request(app)
      .post('/session')
      .set('ios', true)
      .set('api-mewe', true)
      .send({ email,
        password,
        role
      })
      .expect(200)
      .then((response) => {
        this.agentId = response.body.agentId
        this.publicKey = response.body.public_key
        this.secretKey = response.body.secret_key
        return this
      })
  }
  logout () {
    return this.del('/session')
  }
  get secret () {
    const salt = bcrypt.genSaltSync(10)
    // console.log('salt: ', salt)
    let retBcrypt = bcrypt.hashSync(this.secretKey, salt)
    // console.log('public: ', this.publicKey)
    // console.log('secret: ', retBcrypt)
    return retBcrypt
    // return bcrypt.hashSync(this.secretKey, salt)
  }
  request (type, url, data, app) {
    const req = request.agent(this.server)[type](url)
      .set('key', this.publicKey)
      .set('secret', this.secret)
      .set('ios', true)
      .set('api-mewe', true)

    if (data) {
      let action = 'send'
      if (type === 'get') action = 'query'
      req[action](data)
    }

    return req
  }

  get (url, app) {
    return request(app).get(url)
      .set('key', this.publicKey)
      .set('secret', this.secret)
      .set('ios', true)
      .set('api-mewe', true)
  }

  post (url, payload, app) {
    return request(app).post(url)
      .set('key', this.publicKey)
      .set('secret', this.secret)
      .set('ios', true)
      .set('api-mewe', true)
      .send(payload)
  }
  put (url, payload, app) {
    return request(app).post(url)
      .set('key', this.publicKey)
      .set('secret', this.secret)
      .set('ios', true)
      .set('api-mewe', true)
      .send(payload)
  }
}
module.exports = SessionApp
module.exports.defaults = SessionApp
