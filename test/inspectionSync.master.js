
var fs = require('fs')
var bcrypt = require('bcrypt')
var assert = require('assert')
var request = require('supertest-as-promised')
var http = require('http')
var _ = require('lodash')
const casual = require('casual')
const moduleTest = require('../module.js')
var getSession = require('../session.js')
var SessionApp = require('../session2.js')
var sdeMenu = require('../testHelper.js')
let inspectionJson = require('./inspection.master.json')
let sideMenu = new sdeMenu()
// const app = 'https://testing.mewe.org'
// const app = 'https://jeff.coinspectapp.com'
// const app = 'http://localhost:1337'
// const app = 'https://testing.mewe.org'
// const app = 'https://master.stg.coinspectapp.com'
// const app = 'https://testing-tgif.mewe.org'
// const app = 'https://cass.mewe.org'
// const app = 'https://joshua-development007.dev.coinspectapp.com'
const app = 'https://demo9.mewe.org'
// const app = 'https://testing.stg.coinspectapp.com'

const admin = {
  // "email":"admin@dev.mewe.org",
  // "password":"3pTXWMskLzR8kIDG"
  // 'email': 'admintestauto',
  'email': 'admindeltacotest',
  // 'email': 'admindev',
  // 'email': 'adminjeff',
  // 'email': 'cassadmin',
  // 'email': 'admin@testing.mewe.org',
  // 'password': 'password1'
  'password': 'Password1!'
}
const agent = {
  // 'email': 'iosmaster',
  // 'email': 'autoagent',
  // 'email': 'bugtest0',
  // 'email': 'bugdev',
  // 'email': 'iosdev',
  // 'email': 'kei_dev',
  // 'email': 'kei_test',
  // 'email': 'demo9loadtest',
  'email': 'bugdemo9.2',
  // 'email': 'bugmaster5',
  // 'email': 'test270',
  // 'email': 'appcheck',
  // 'email': 'autotest2',
  // 'email':'testagent',
  // 'email': 'testdd',
  // 'email': 'casstess',
  // 'email': 'mastercheck',
  'password': 'Password1!'
  // 'password': 'password'
  // 'password': 'password1'
}

/* endPoints */
const endPoints = {
  // get:
  buildingFilter: '/building/search?getUnits=true&keyword=a', // added by nUnO to filter buildings
  inspectionDownload: '/inspection/download/', // '/inspection/download/1'
  inspectionFind: '/inspection/find/', // '/inspection/find/1
  inspectionLatest: '/inspection/latest',
  checklistList: '/checklist/list',
  checklists: '/checklists',
  checklistFind: '/checklist/find/', // '/inspection/find/1'
  checklistByGroup: '/checklist/byusergroup?propertyId=',
  categoryFind: '/category/list',
  buildingFind: '/building/find/', // /building/find/1
  buildingHistory: '/building/', // '/building/1/history'
  buildingSearch: '/building/search', // to display all buildings associated to the user
  search: '/search',
  settingFind: '/setting/find',
  inspectionActivities: '/inspection/activities',

  // post: 
  // addPermission: '/usergroup/173/addpermissions',
  // payload ex: {"type":"property","ids":[1225]}

  createInspection: '/inspection/create',
  inspectionSync: '/inspection/sync',
  mediaMobile: '/media/mobile',
  buildingCreate: '/building/create'
}

describe('AllowSync Check', function () {
  this.timeout(50000)
  let currentSession
  async function loadSession (email, password, app2) {
    let session = new SessionApp()
    return await session.login(email, password, app2, 'AGENT')
  }

  before(async () => {
    // currentSession = await loadSession(admin.email, 'password', app)
    currentSession = await loadSession(agent.email, agent.password, app)
  })

  it.only('inspection/sync', async () => { // Create Inspections
    let syncPayload = getData()
    // console.log(syncPayload[0])
    let initialLatest = await currentSession.get('/inspection/latest?result=false', app)
    console.log('initialLatest.body.response.Length: ', initialLatest.body.response.length)
    let previousCount = initialLatest.body.response.length

    let resInspection = await currentSession.put(endPoints.inspectionSync, syncPayload, app)
    let resID = resInspection.body.response[0]
    console.log(resInspection.body)
    console.log('yoyo: ', resID)
    let latest = await currentSession.get('/inspection/latest?result=false', app)
    let updatedAtPayLoad = syncPayload[0].updatedAt
    let updatedAtServer = latest.body.response[0].updatedAt
    console.log('PayloadID: ', resID)
    console.log('ServerID : ', latest.body.response[0].id)
    // console.log(latest.body)
    console.log('mobile updatedAt: ', updatedAtPayLoad)
    console.log('server updatedAt: ', updatedAtServer)

    // assert.equal(updatedAtServer, updatedAtPayLoad)
  })// .timeout(9999999)

  function getData () {
  //   let add = count * 1000;
  // let createdAt = (new Date(+start + add)).toISOString();
  // let updatedAt = (new Date(+ start + add + 1000)).toISOString();
    return [inspectionJson] // end result
  }
})
