
var bcrypt = require('bcrypt')
var assert = require('assert')
var request = require('supertest-as-promised')
var http = require('http')
var _ = require('lodash')
const moduleTest = require('../module.js')
var getSession = require('../session.js')
var SessionApp = require('../session2.js')
var sdeMenu = require('../testHelper.js')
let sideMenu = new sdeMenu()
const app = 'https://testing.mewe.org'
// const app = 'http://localhost:1337'
const admin = {
  // "email":"admin@dev.mewe.org",
  // "password":"3pTXWMskLzR8kIDG"
  'email': 'admin@testing.mewe.org',
  'password': 'password1'
}
const agent = {
  // 'email': 'autoagent',
  // 'email': 'bugtest',
  'email': 'bugdev',
  // 'email': 'autotest2',
  // 'email':'testagent',
  'password': 'password1'
}

/* endPoints */
const endPoints = {
  // get:
  buildingFilter: '/building/search?getUnits=true&keyword=a', // added by nUnO to filter buildings
  inspectionDownload: '/inspection/download/', // '/inspection/download/1'
  inspectionFind: '/inspection/find/', // '/inspection/find/1
  inspectionLatest: '/inspection/latest',
  checklistList: '/checklist/list',
  checklists: '/checklists',
  checklistFind: '/checklist/find/', // '/inspection/find/1'
  checklistByGroup: '/checklist/byusergroup?propertyId=',
  categoryFind: '/category/list',
  buildingFind: '/building/find/', // /building/find/1
  buildingHistory: '/building/', // '/building/1/history'
  buildingSearch: '/building/search', // to display all buildings associated to the user
  search: '/search',
  settingFind: '/setting/find',
  inspectionActivities: '/inspection/activities',

  // post: 
  inspectionSync: '/inspection/sync',
  mediaMobile: '/media/mobile',
  buildingCreate: '/building/create'
}

describe('test proper', function () {
  this.timeout(20000)
  let prop, currentSession, checkJson
  const inspectionHolder = {
    propertyId: '1',
    checklistId: '2',
    checklistObj: null
  }

  async function loadSession (email, password, app2) {
    let session = new SessionApp()
    return await session.login(email, password, app2)
  }

  before(async () => {
    currentSession = await loadSession(agent.email, 'password', app)
  })

  // var arrTest = ['Func1', 'Func2', 'Func3']
  // describe.only('testtesttestwetestsetes', () => { // test this for APPIUM, para mobo ang code
  //   for (let key in sideMenu) {
  //     it(key, () => {
  //       // if (key !== 'role') UserEditorPage[key].setValue(user[key]);
  //       console.log('testse:' + sideMenu[key])
  //     })
  //   }
  // })

  it('test here', () => {
    let count = 2
    for (let index = 1; index <= count; index++) {
      console.log('Index is: ' + index)
    }
    let testVar = []
    // testVar = 'test'
    console.log(testVar)
    // testVar.push('magic')
    // testVar.push('trick')
    console.log(testVar.length)
    console.log('>>>>>>>>>')
    let xxx = {}
    xxx.a = 'a'
    console.log(xxx.a)

    let sam = [{
      'a': '1',
      'b': '2',
      'c': '3',
      'd': ['arr1', 'arr2']
    },
    {
      'a': '555',
      'b': '2',
      'c': '3',
      'd': ['arr1', 'arr2']
    }]

    for (var index = 0; index < sam.length; index++) {
      var element = sam[index]
    }
    sam.forEach(function (elem) {
      console.log(elem['a'])
    })
  })
  it.only('should list all property for user', async () => {
    let propertyResponse = await currentSession.get(endPoints.buildingSearch, app)
    const propertyArr = propertyResponse.body
    // console.log(propertyArr.body.name)
    propertyArr.forEach(function (element) {
      console.log(element.name.trim(), element.address)
    })
    console.log(propertyResponse.body.length)
    console.log(propertyArr[propertyResponse.body.length - 1].name)
    console.log('--------------------')
    // console.log(propertyArr)
    // response.body.response.forEach(function(elem) {
    // console.log(elem.name)
    // });
    // console.log('Property--: ' + propertyArr[10].name)
    const checklistResponse = await currentSession.get(endPoints.checklistByGroup + propertyArr[0].id, app)

    inspectionHolder.checklistId = checklistResponse.body[0].id
    console.log(checklistResponse.body)
    console.log('--------------------')
    console.log(checklistResponse.body.length)
    console.log(checklistResponse.body[0].name)
    console.log('inspectionHolder.checklistId: ' + inspectionHolder.checklistId)
    const findChecklist = await currentSession.get(`${endPoints.checklistFind}${inspectionHolder.checklistId}`, app)
    inspectionHolder.checklistObj = findChecklist.body.response.categories

    console.log(inspectionHolder.checklistObj.length)
    console.log('--------------------')
    inspectionHolder.checklistObj.forEach(function (element) {
      // console.log('>>> Group Name: ' + element.name)
      console.log(element)
      element.criterias.forEach(function (criteria) {
        // console.log(criteria)
        // criteria
      })
    })

    let jsonSearch = _.find(inspectionHolder.checklistObj, function (o) {
      return o.criteria === 'One Child Answered by No [Yes | No]'
    })
    console.log('<<<<<<<<<<<<<<<<<')
    console.log(jsonSearch)
    console.log('<<<<<<<<<<<<<<<<<')
    let jsonFInd = _(inspectionHolder.checklistObj).map('criterias').flatten().find(['index', '1.0'], ['criteria', 'Question 1 with 2 sub [Yes | No]'])

    console.log(jsonFInd)
    console.log('++++++++++++++++++++')
    let aaa = _.filter(jsonFInd.options, ['criteriaId', '43041'])
    // let aaa = _.find(jsonFInd.options, ['criteriaId', '43041'])
    console.log(aaa)
    console.log('++++++++++++++++++++')
    let bbb = jsonFInd.options.filter(function (o) {
      return o.criteriaId === 43041
    })
    jsonFInd.options.forEach(function (x) {
      console.log(x['label'])
    })

    for (var key = 0; key < inspectionHolder.checklistObj.length; key++) {
      // console.log(key)
      // console.log(inspectionHolder.checklistObj[key].criterias)
      // console.log(inspectionHolder.checklistObj)

      // console.log(inspectionHolder.checklistObj[key].name)
    }

    // for (var key in Object.keys(inspectionHolder.checklistObj)) {
    // console.log(inspectionHolder.checklistObj[key])
    // }
  })

  it.skip('should get list of checklist per property', (done) => {
    console.log(prop.id)
    console.log(prop.name)
    currentSession.get(endPoints.checklistByGroup + prop.id, app)
      .then(function (response) {
        console.log(response.body)
        checkJson = response.body
        console.log(checkJson[0])
        done()
      })
  })

  it.skip('should get Checklist', (done) => {
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        console.log('---- Checklist with Group----')
        console.log(response.body.response)
        done()
      })
  })
  // MAKE SOMETHING OR FUNCTION THAT WILL RETURN CATEGORY / CRITERIA / OPTION
  it.skip('should get Category for checklist as per property / user', (done) => { // MAO NI SIYA PADAGANA
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        let catJson = response.body.response.categories
        catJson.forEach(function (category) {
          // console.log(category)
          category.criterias.forEach(function (criteria) {
            console.log(criteria)
          })
        })
        // console.log(response.body.response.categories.length)
        // console.log('---- Criterias per Group----')
        // console.log(response.body.response.categories[0])
        done()
      })
  })

  it.skip('should get checklist for Criteria', (done) => {
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        // console.log(response.body.response.categories[0].criterias.options.length)
        console.log('---- Criterias Options----')
        console.log(response.body.response.categories[0].criterias.options)
        done()
      })
  })

  function ret () {

  }

  it.skip('test random', () => { // this is how to access object with array
    const property = [{
      name: 'Prop name',
      addresss: 'addresss1'
    }]
    const person = {
      name: [{ fname: 'Fname' }],
      address: [{ name: 'Name prop', address: 'add 1' }]
    }
    console.log(person.name[0].fname)
  })
})
