
var fs = require('fs')
var bcrypt = require('bcrypt')
var assert = require('assert')
var request = require('supertest-as-promised')
var http = require('http')
var _ = require('lodash')
const casual = require('casual')
const moduleTest = require('../module.js')
var getSession = require('../session.js')
var SessionApp = require('../session2.js')
var sdeMenu = require('../testHelper.js')
let sideMenu = new sdeMenu()
// const app = 'https://testing.mewe.org'
// const app = 'http://localhost:1337'
// const app = 'https://testing.mewe.org'
// const app = 'https://master.mewe.org'
const app = 'https://' + process.env.npm_config_app + '.mewe.org'
const admin = {
  // "email":"admin@dev.mewe.org",
  // "password":"3pTXWMskLzR8kIDG"
  // 'email': 'admintestauto',
  // 'email': 'adminmaster',
  'email': process.env.npm_config_username,
  // 'email': 'admin@testing.mewe.org',
  'password': 'password1'
}
const agent = {
  // 'email': 'iosmaster',
  // 'email': 'autoagent',
  // 'email': 'bugtest0',
  // 'email': 'bugdev',
  // 'email': 'iosdev',
  // 'email': 'appcheck',
  // 'email': 'autotest2',
  // 'email':'testagent',
  'email': 'testdd',
  'password': 'password1'
}

/* endPoints */
const endPoints = {
  // get:
  buildingFilter: '/building/search?getUnits=true&keyword=a', // added by nUnO to filter buildings
  inspectionDownload: '/inspection/download/', // '/inspection/download/1'
  inspectionFind: '/inspection/find/', // '/inspection/find/1
  inspectionLatest: '/inspection/latest',
  checklistList: '/checklist/list',
  checklists: '/checklists',
  checklistFind: '/checklist/find/', // '/inspection/find/1'
  checklistByGroup: '/checklist/byusergroup?propertyId=',
  categoryFind: '/category/list',
  buildingFind: '/building/find/', // /building/find/1
  buildingHistory: '/building/', // '/building/1/history'
  buildingSearch: '/building/search', // to display all buildings associated to the user
  search: '/search',
  settingFind: '/setting/find',
  inspectionActivities: '/inspection/activities',

  // post: 
  createInspection: '/inspection/create',
  inspectionSync: '/inspection/sync',
  mediaMobile: '/media/mobile',
  buildingCreate: '/building/create'
}

describe('test proper', function () {
  this.timeout(20000)
  let prop, currentSession, checkJson
  const inspectionHolder = {
    propertyId: '1',
    checklistId: '2',
    checklistObj: null
  }

  async function loadSession (email, password, app2) {
    console.log(app)
    console.log(admin.email)
    let session = new SessionApp()
    return await session.login(email, password, app2)
  }

  before(async () => {
    currentSession = await loadSession(admin.email, 'password', app)
  })

  // var arrTest = ['Func1', 'Func2', 'Func3']
  // describe.only('testtesttestwetestsetes', () => { // test this for APPIUM, para mobo ang code
  //   for (let key in sideMenu) {
  //     it(key, () => {
  //       // if (key !== 'role') UserEditorPage[key].setValue(user[key]);
  //       console.log('testse:' + sideMenu[key])
  //     })
  //   }
  // })
  it.only('yo yo oyo', async () => {
    // https://testing.mewe.org/building/search?categoryId=216&limit=10&skip=10
    let propList = await currentSession.get('/building/search?categoryId=216', app)
    // console.log(propList.body)
    console.log('yeah: ')
  })

  it.skip('Create Properties', async () => { // Create Inspections
    let bodyID
    let catPropID = process.env.npm_config_catID
    console.log('catid: ' + catPropID)
    // to run 
    // npm run test2 --catID=<propertyCategoryID>, --username=<adminusername, --app=<serverURL>
    // npm run test2 --catID=271 --username=adminmaster --app=master
    this.timeout(500000)
    for (let index = 1; index <= 1; index++) {
      let payload = {
        'categoryId': catPropID,
        'pictures': [],
        'name': '#' + index + ' ' + casual.street,
        'description': '',
        'address': casual.address1,
        'address2': '',
        'latitude': 0,
        'longitude': 0,
        'city': casual.city,
        'state': casual.state,
        'zip': 1234,
        'country': casual.country,
        'owner': '',
        'timezone': 'Asia/Manila',
        'photos': []
      }

      let createProp = await currentSession.post(endPoints.buildingCreate, payload, app)

        .then((res) => {
          bodyID = res.body.id
          // console.log(res.body.id)
        })
      console.log('#' + index + ' ' + bodyID + ': ' + payload.name + ', ' + payload.address + ', ' + payload.city + ', ' + payload.state)
    }
    // console.log('yeah 2: ' + process.env.npm_config_yo1)
    // console.log('yeah 2: ' + process.env.npm_config_yo2)
    // fs.writeFile('inspectionBody.txt', findIns[0])
  })// .timeout(9999999)

  it.skip('create inspection', async () => { // Create Inspections
    let bodyID
    this.timeout(500000)
    for (let index = 0; index < 1; index++) {
      let createIns = await currentSession.post(endPoints.createInspection, {'buildingId': 139, 'checklistId': 723}, app)
        // .set('api-mewe', true)
        // .send(
        //   {
        //     'buildingId': 139, 'checklistId': 723
        //   }
        // )
        .then((res) => {
          bodyID = res.body.id
          // console.log(res.body.id)
        })
      fs.appendFile('response.txt', bodyID + '\n')
      // console.log('createIns: ' + createIns)

      // find inspections
      let findIns
      console.log(index + 1, '. InspectionID: ' + bodyID)
      let yooo = await currentSession.get(endPoints.inspectionFind + bodyID, app)
      // let yooo = await currentSession.get('/inspection/start/' + bodyID, app)
        .then((res) => {
          findIns = res
          // console.log(res.body)
          findIns = res.body
        })
    }

    // fs.writeFile('inspectionBody.txt', findIns[0])
  })

  it.skip('test here', () => {
    let count = 2
    for (let index = 1; index <= count; index++) {
      console.log('Index is: ' + index)
    }
    let testVar = []
    // testVar = 'test'
    console.log(testVar)
    // testVar.push('magic')
    // testVar.push('trick')
    console.log(testVar.length)
    console.log('>>>>>>>>>')
    let xxx = {}
    xxx.a = 'a'
    console.log(xxx.a)

    let sam = [{
      'a': '1',
      'b': '2',
      'c': '3',
      'd': ['arr1', 'arr2']
    },
    {
      'a': '555',
      'b': '2',
      'c': '3',
      'd': ['arr1', 'arr2']
    }]

    for (var index = 0; index < sam.length; index++) {
      var element = sam[index]
    }
    sam.forEach(function (elem) {
      console.log(elem['a'])
    })
  })

  it.skip('should list all property for user', async () => {
    let activitiesResponse = await currentSession.get(endPoints.inspectionActivities, app)
    // const propertyArr = propertyResponse.body

    // console.log(activitiesResponse.body)
    // console.log(propertyArr.body)

    // console.log(propertyArr.body.name)
    // propertyArr.forEach(function (element) {
    //   console.log(element.name.trim(), element.address)
    // })

    // console.log(propertyArr)
    // response.body.response.forEach(function(elem) {
    // console.log(elem.name)
    // });
    // console.log('Property--: ' + propertyArr[10].name)
  })

  it.skip('should list all property for user', async () => {
    let propertyResponse = await currentSession.get(endPoints.buildingSearch, app)
    const propertyArr = propertyResponse.body
    // console.log(propertyArr.body.name)
    propertyArr.forEach(function (element) {
      console.log(element.name.trim(), element.address)
    })
    console.log(propertyResponse.body.length)
    console.log(propertyArr[propertyResponse.body.length - 1].name)
    console.log('--------------------')
    // console.log(propertyArr)
    // response.body.response.forEach(function(elem) {
    // console.log(elem.name)
    // });
    // console.log('Property--: ' + propertyArr[10].name)
    const checklistResponse = await currentSession.get(endPoints.checklistByGroup + propertyArr[0].id, app)

    inspectionHolder.checklistId = checklistResponse.body[0].id
    console.log(checklistResponse.body)
    console.log('--------------------')
    console.log(checklistResponse.body.length)
    console.log(checklistResponse.body[0].name)
    console.log('inspectionHolder.checklistId: ' + inspectionHolder.checklistId)
    const findChecklist = await currentSession.get(`${endPoints.checklistFind}${inspectionHolder.checklistId}`, app)
    inspectionHolder.checklistObj = findChecklist.body.response.categories

    console.log(inspectionHolder.checklistObj.length)
    console.log('--------------------')
    inspectionHolder.checklistObj.forEach(function (element) {
      // console.log('>>> Group Name: ' + element.name)
      console.log(element)
      element.criterias.forEach(function (criteria) {
        // console.log(criteria)
        // criteria
      })
    })

    let jsonSearch = _.find(inspectionHolder.checklistObj, function (o) {
      return o.criteria === 'One Child Answered by No [Yes | No]'
    })
    console.log('<<<<<<<<<<<<<<<<<')
    console.log(jsonSearch)
    console.log('<<<<<<<<<<<<<<<<<')
    let jsonFInd = _(inspectionHolder.checklistObj).map('criterias').flatten().find(['index', '1.0'], ['criteria', 'Question 1 with 2 sub [Yes | No]'])

    console.log(jsonFInd)
    console.log('++++++++++++++++++++')
    let aaa = _.filter(jsonFInd.options, ['criteriaId', '43041'])
    // let aaa = _.find(jsonFInd.options, ['criteriaId', '43041'])
    console.log(aaa)
    console.log('++++++++++++++++++++')
    let bbb = jsonFInd.options.filter(function (o) {
      return o.criteriaId === 43041
    })
    jsonFInd.options.forEach(function (x) {
      console.log(x['label'])
    })

    for (var key = 0; key < inspectionHolder.checklistObj.length; key++) {
      // console.log(key)
      // console.log(inspectionHolder.checklistObj[key].criterias)
      // console.log(inspectionHolder.checklistObj)

      // console.log(inspectionHolder.checklistObj[key].name)
    }

    // for (var key in Object.keys(inspectionHolder.checklistObj)) {
    // console.log(inspectionHolder.checklistObj[key])
    // }
  })

  it.skip('should get list of checklist per property', (done) => {
    console.log(prop.id)
    console.log(prop.name)
    currentSession.get(endPoints.checklistByGroup + prop.id, app)
      .then(function (response) {
        console.log(response.body)
        checkJson = response.body
        console.log(checkJson[0])
        done()
      })
  })

  it.skip('should get Checklist', (done) => {
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        console.log('---- Checklist with Group----')
        console.log(response.body.response)
        done()
      })
  })
  // MAKE SOMETHING OR FUNCTION THAT WILL RETURN CATEGORY / CRITERIA / OPTION
  it.skip('should get Category for checklist as per property / user', (done) => { // MAO NI SIYA PADAGANA
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        let catJson = response.body.response.categories
        catJson.forEach(function (category) {
          // console.log(category)
          category.criterias.forEach(function (criteria) {
            console.log(criteria)
          })
        })
        // console.log(response.body.response.categories.length)
        // console.log('---- Criterias per Group----')
        // console.log(response.body.response.categories[0])
        done()
      })
  })

  it.skip('should get checklist for Criteria', (done) => {
    currentSession.get(endPoints.checklistFind + '357', app)
      .then(function (response) {
        // console.log(response.body.response.categories[0].criterias.options.length)
        console.log('---- Criterias Options----')
        console.log(response.body.response.categories[0].criterias.options)
        done()
      })
  })

  function ret () {

  }

  it.skip('test random', () => { // this is how to access object with array
    const property = [{
      name: 'Prop name',
      addresss: 'addresss1'
    }]
    const person = {
      name: [{ fname: 'Fname' }],
      address: [{ name: 'Name prop', address: 'add 1' }]
    }
    console.log(person.name[0].fname)
  })
})
