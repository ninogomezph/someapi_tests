var request = require("supertest-as-promised");
// const app = 'https://testing.mewe.org'
var bcrypt = require('bcrypt');

function getSession(email, password,url,app)
	{

		return request(app).post('/session') // this can be placed in the before hook (login | session)
				.set('ios',true)
				.set('api-mewe',true)
				.send({'email': email, 'password':password})
				.expect(200)
				.then(function (res) {
					resBody = res.body
					const salt = bcrypt.genSaltSync(10);
					secretKey = bcrypt.hashSync(resBody.secret_key, salt);
					publicKey = res.body.public_key
					// console.log(resBody.profile.email)
					// console.log('========')
			})
	}
// function getSession2(email, password,url,app)
// 	{

// 		return request(app).post('/session') // this can be placed in the before hook (login | session)
// 				.set('ios',true)
// 				.set('api-mewe',true)
// 				.send({'email': email, 'password':password})
// 				.expect(200)
// 				.then(function (res) {
// 					resBody = res.body
// 					const salt = bcrypt.genSaltSync(10);
// 					secretKey = bcrypt.hashSync(resBody.secret_key, salt);
// 					publicKey = res.body.public_key
// 					// console.log(resBody.profile.email)
// 					// console.log('========')
// 			})
// 				.then(function () {
// 						return request(app).get(url)
// 						// request(app).get('/checklist/find/357')  // 478 TGIF Brand Review | 357 simple checklist
// 						.set('ios',true)
// 						.set('api-mewe',true)
// 						.set('secret',secretKey)
// 						.set('key',publicKey)
// 						// .then(function (ress) {
// 			})

// 	}
module.exports = getSession