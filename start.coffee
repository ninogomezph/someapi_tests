module.exports = (req,res)->
  inspectionId = +(req.param "id")

  if inspectionId and !isNaN(inspectionId)
    Inspection.updateProgress inspectionId
    .then (data)->
      sails.log.info("Inspection started: #{inspectionId || ''}")
      Email.sendNotification (req.param "id"), "started", "https://" + req.headers.host
      .then (data) ->
        res.json {
          succes: true
        }
      .catch (err)->
        res.json err
  else
    res.json {
      success: false,
      error: "Inspection id not defined."
    }
